module.exports = {
    configureWebpack: {
        module: {
            rules: [
                {
                    test: /\.csv$/,
                    loader: "csv-loader",
                    options: {
                        dynamicType: true,
                        header: true,
                        skipEmptyLines: true
                    }
                }
            ]
        }
    }
};
