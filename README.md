# Knockout Stream Leaderboard

## Requirements

- Node LTS

## Installation instructions

- Make a `data.csv` file. (An example format is in `example_data.csv`)
- In the project root, run `npm i` or `yarn`
- In the project root, run `npm run build` or `yarn build`
- The compiled assets are now in the `dist/` folder.
