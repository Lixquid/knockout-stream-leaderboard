import Vue from "vue";
import VueRouter, { RouteConfig } from "vue-router";
import Home from "../views/Home.vue";
import Leaderboard from "../views/Leaderboard.vue";

Vue.use(VueRouter);

const routes: Array<RouteConfig> = [
    {
        path: "/",
        name: "Home",
        component: Home
    },
    {
        path: "/leaderboard",
        name: "Leaderboard",
        component: Leaderboard
    }
];

const router = new VueRouter({
    routes
});

export default router;
