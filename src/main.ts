import "@fortawesome/fontawesome-free/css/fontawesome.min.css";
import "@fortawesome/fontawesome-free/css/solid.min.css";
import VueCompositionApi from "@vue/composition-api";
import "bootstrap/dist/css/bootstrap.min.css";
import Vue from "vue";
import { ClientTable } from "vue-tables-2";
import App from "./App.vue";
import SortControl from "./components/SortControl.vue";
import router from "./router";

Vue.use(VueCompositionApi);
Vue.use(ClientTable, {}, false, "bootstrap4", {
    sortControl: SortControl
});

Vue.config.productionTip = false;

new Vue({
    router,
    render: h => h(App)
}).$mount("#app");
