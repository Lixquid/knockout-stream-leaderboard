declare module "@/../data.csv" {
    export interface IDataEntry {
        readonly date: string;
        readonly game: string;
        readonly winner: string;
        readonly winner2: string;
        readonly winner3: string;
        readonly artefact_url: string;
    }

    const data: ReadonlyArray<IDataEntry>;
    export default data;
}
