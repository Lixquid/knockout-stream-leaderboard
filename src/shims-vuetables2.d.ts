declare module "vue-tables-2" {
    import { VueConstructor } from "vue";

    export const ClientTable: (vue: VueConstructor) => void;
}
